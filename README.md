#  Interacting with Amazon Bedrock Model through API Gateway and Lambda Functions

My posts on Medium about the project:

[Interacting with Amazon Bedrock Model through API Gateway and Lambda Functions](https://medium.com/p/0ba51436fd02)

[Implementing Tracing with AWS X-Ray service and AWS X-Ray SDK for REST API Gateway and Lambda functions](https://medium.com/p/810eb012383d)

## Prerequisites

Before you start, make sure the following requirements are met:
- An AWS account with permissions to create resources.
- [AWS CLI](https://aws.amazon.com/cli/) installed on your local machine.

## Deployment

1. **Clone the repository.**

2. **Set up AWS Bedrock model.**

Go to the Amazon Bedrock service in the AWS Console.
Navigate to Get Started -> Request model access -> Modify model access -> Choose the appropriate model available in your region (e.g., Titan Text G1 - Express) -> Next -> Submit. 
Wait a few minutes and refresh the page to see "Access granted".
Navigate to Overview -> choose Provider of the available model -> choose the model -> see the API request of the model.

3. **Create authorization token in AWS Systems Manager Parameter Store.**

```
aws ssm put-parameter --name "AuthorizationLambdaToken" --value "token_value_secret" --type "SecureString"
```

4. **Fill in all necessary Parameters in root.yaml CloudFormation template, and retrieve_invoke_url.sh script, and create the CloudFormation stack.**

```
aws cloudformation create-stack \
    --stack-name apigw-lambda-bedrock \
    --template-body file://infrastructure/root.yaml \
    --capabilities CAPABILITY_NAMED_IAM \
    --disable-rollback
```

5. **Retrieve the Invoke URL of the Stage using the retrieve_invoke_url.sh script.**

```
./scripts/retrieve_invoke_url.sh
```

6. **Test the Bedrock model, Main Lambda function, and API.**

```
aws bedrock-runtime invoke-model \
    --model-id "amazon.titan-text-express-v1" \
    --content-type "application/json" \
    --accept "application/json" \
    --body "$(echo -n '{"inputText": "What is the capital of Ukraine?", "textGenerationConfig": {"maxTokenCount": 8192, "stopSequences": [], "temperature": 0, "topP": 1}}' | base64)" \
    output.json > /dev/null && cat output.json | jq -r '.results[0].outputText' && rm output.json

aws lambda invoke \
    --function-name MainLambdaFunction \
    --payload '{"queryStringParameters": {"inputText": "What is the capital of Poland?"}}' \
    --cli-binary-format raw-in-base64-out \
    response.json > /dev/null && cat response.json | jq -r '.body | fromjson | .results[0].outputText' && rm response.json

export APIGW_TOKEN='token_value'
curl -s -X GET -H "Authorization: Bearer $APIGW_TOKEN" "https://api_id.execute-api.eu-central-1.amazonaws.com/dev/invoke?inputText=What%20is%20the%20capital%20of%20Germany?" | jq -r '.results[0].outputText'
```

7. **Delete token from SM Parameter Store, and the CloudFormation stack.**

```
aws ssm delete-parameter --name "AuthorizationLambdaToken"

aws cloudformation delete-stack --stack-name apigw-lambda-bedrock
```

## Enable tracing with AWS X-ray (optionally).

1. **Create lambda layer with aws-xray-sdk library and next use the lambda layer version ARN in tracing-rest-api.yaml.**

```
aws lambda publish-layer-version --layer-name aws-xray-sdk-layer --zip-file fileb://infrastructure/aws-xray-sdk-layer-layer/aws-xray-sdk-layer-layer.zip --compatible-runtimes python3.12
```

2. **Update current CloudFormation stack - add X-ray tracing and change HTTP on REST api.**

```
aws cloudformation update-stack \
    --stack-name apigw-lambda-bedrock \
    --template-body file://infrastructure/tracing-rest-api.yaml \
    --capabilities CAPABILITY_NAMED_IAM \
    --disable-rollback
```

3. **Retrieve the Invoke URL of the Stage using the retrieve_invoke_url_rest_api.sh script and test with curl.**

```
./scripts/retrieve_invoke_url_rest_api.sh

export APIGW_TOKEN='token_value'
curl -s -X GET -H "Authorization: Bearer $APIGW_TOKEN" "https://api_id.execute-api.eu-central-1.amazonaws.com/dev/invoke?inputText=What%20is%20the%20capital%20of%20Germany?" | jq -r '.results[0].outputText'
```
4. **Check X-Ray trace map and segments timeline.**

To view the X-Ray trace map and segments timeline, navigate to the AWS Console: CloudWatch -> X-Ray traces -> Traces. In the Traces section, a list of trace IDs will be displayed. Clicking on a trace ID will reveal the Trace map, Segments timeline, and associated Logs.

5. **Delete the lambda layer version.**

```
aws lambda delete-layer-version --layer-name aws-xray-sdk-layer --version-number <number>
```

## Infrastructure schema and test results

*Infrastructure schema*

![schema](images/apigw_lambda_bedrock.png)

*Tests of the Bedrock model, Main Lambda function, and API from CLI*

![test_cli](images/test_requests.png)

*Test of the API from Postman*

![test_postman](images/api_request_postman.png)

*Trace map from the updated infrastructure*

![trace_map](images/xray_trace_map.png)

You can support me with a virtual coffee https://www.buymeacoffee.com/andrworld1500 .